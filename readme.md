# CN Cypress Demo
A demo of Cypress testing against the CN site

## Executing Tests
### Install NPM and Node.js
https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

### Install Dependencies
    // Open a terminal in the root of the project /rs-cypress-demo
    npm install

### Execute All Tests
    // Open a terminal in the root of the project /rs-cypress-demo
    // With Visual Test Runner
    npx cypress open

    // Headlessly
    npx cypress run