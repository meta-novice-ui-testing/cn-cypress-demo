describe('Join Page', () => {
  beforeEach(() => {
    cy.visit('/join');
  });

  it('should have all visible elements necessary to join', () => {
    cy.get('[data-test="name"]').should('be.visible');
    cy.get('[data-test="email"]').should('be.visible');
    cy.get('[data-test="password"]').should('be.visible');
    cy.get('[data-test="confirm-password"]').should('be.visible');
    cy.get('[data-test="accept-eula"]').should('be.visible');
    cy.get('[data-test="signup"]').should('be.visible');
  });

  it('should allow a user with valid information to join', () => {
    cy.generateNewTestUserJson().then((newUser) => {
      cy.fillOutJoinForm(newUser);
      cy.get('[data-test="accept-eula"]').click();
      cy.get('[data-test="signup"]').click();
      cy.contains('Welcome to Clinician Nexus!');
    });
  });

  it('should provide a useful message if the eula is not accepted', () => {
    cy.generateNewTestUserJson().then((newUser) => {
      cy.fillOutJoinForm(newUser);
      cy.get('[data-test="signup"]').click();
      cy.contains('You must accept the end user license agreement to continue');
    });
  });

  // ToDo: Test form field validations: Name, Email, Password, Confirm Password
  // ToDo: Test Password Mismatch
});