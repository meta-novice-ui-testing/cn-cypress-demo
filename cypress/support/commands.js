Cypress.Commands.add('generateTestEmail', () => {
    var testEmail = 'cn_test_user_' + Math.floor((Math.random() * 100000) + 1) + '@sharklasers.com';
    cy.wrap(testEmail);
});

Cypress.Commands.add('generateNewTestUserJson', () => {
    cy.fixture('join/new_user.json').then((newUser) => {
        cy.generateTestEmail().then(email => {
            newUser.email = email;
            cy.wrap(newUser);
        });
    });
});
  
  