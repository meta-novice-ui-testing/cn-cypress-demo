Cypress.Commands.add('fillOutJoinForm', (user) => {
    cy.get('[data-test="name"]').type(user.name);
    cy.get('[data-test="email"]').type(user.email);
    cy.get('[data-test="password"]').type(user.password);
    cy.get('[data-test="confirm-password"]').type(user.password);
});